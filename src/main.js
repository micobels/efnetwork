// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'

// carousel 
import VueCarousel from 'vue-carousel';

// smooth scroll function
import vueSmoothScroll from 'vue2-smooth-scroll'

//slider
import EasySlider from 'vue-easy-slider'


Vue.use(vueSmoothScroll) 
Vue.use(VueCarousel);
Vue.use(EasySlider)

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
